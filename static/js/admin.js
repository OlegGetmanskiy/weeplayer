var player;
var duration = 0;

var spinOptions = {
   lines: 15, // The number of lines to draw
   length: 20, // The length of each line
   width: 5, // The line thickness
   radius: 20, // The radius of the inner circle
   corners: 1, // Corner roundness (0..1)
   rotate: 0, // The rotation offset
   direction: 1, // 1: clockwise, -1: counterclockwise
   color: '#000', // #rgb or #rrggbb or array of colors
   speed: 1, // Rounds per second
   trail: 45, // Afterglow percentage
   shadow: false, // Whether to render a shadow
   hwaccel: false, // Whether to use hardware acceleration
   zIndex: 2e9, // The z-index (defaults to 2000000000)
   top: '50%', // Top position relative to parent
   left: '50%' // Left position relative to parent
}

var locks = {
   
}

var templates = {
   adminStats : null,
   usersList: null,
}

var usersTable = {
   template : null,
   data : [],
   currentPage : 0,
   pageSize : 50,
   init : function() {
      this.template = Tempo.prepare('template-users-list');
      this.nextPage();
   },
   clear : function() {
      this.data = [];
      this.template.clear();
   },
   append : function(users) {
      for(var i = 0; i < users.length; i++)
         this.data.push(users[i]);
      this.template.render(this.data);
   },
   appendOne : function(user) {
      this.data.push(user);
      this.template.render(this.data);
   },
   deleteByEmail : function(email) {
      alertify.confirm('Really delete '+email+'?', function (e) {
         if (e) {
            $.post('api/admin/users/remove', { email: email }, function(data) {
               for(var i = 0; i < usersTable.data.length; i++) {
                  if(email == usersTable.data[i].email)
                     usersTable.data.splice(i, 1);
               }
               usersTable.template.render(usersTable.data);
            });
         }
      });
   },
   nextPage : function() {
      $.post('api/admin/users', { page: this.currentPage, size: this.pageSize }, function(data) {
         alertErrors(data.errors);
         usersTable.append(data.users);
         if(data.users.length > 0)
            usersTable.currentPage++;
         else {
            $('#users-load-more').removeClass('button-common');
            $('#users-load-more').addClass('text-regular');
            $('#users-load-more').html('(end of list)');
            $('#users-load-more').css({ margin: '0 2em' });
         }
      })
   }
}

var invitesTable = {
   template : null,
   data : [],
   currentPage : 0,
   pageSize : 50,
   init : function() {
      this.template = Tempo.prepare('template-invites-list');
      this.nextPage();
   },
   clear : function() {
      this.data = [];
      this.template.clear();
   },
   append : function(invites) {
      for(var i = 0; i < invites.length; i++)
         this.data.push(invites[i]);
      this.template.render(this.data);
   },
   appendOne : function(invite) {
      this.data.push(invite);
      this.template.render(this.data);
   },
   deleteByEmail : function(email) {
      alertify.confirm('Really delete '+email+'?', function(e) {
         if(e) {
            $.post('api/admin/invites/remove', { email: email });
            for(var i = 0; i < invitesTable.data.length; i++) {
               if(email == invitesTable.data[i].email)
                  invitesTable.data.splice(i, 1);
            }
            invitesTable.template.render(invitesTable.data);
         }
      });
      
   },
   add : function() {
      alertify.prompt('Enter email', function(e, email) {
         if(e) {
            alertify.prompt('Enter code phrase', function( e, codePhrase) {
               if(e) {
                  $.post('api/admin/invites/add', { email: email, codePhrase: codePhrase }, function(data) {
                     var _this = invitesTable;
                     alertErrors(data.errors);
                     if(!data.errors || data.errors.length == 0) {
                        _this.appendOne({ email: email, codePhrase: codePhrase });
                     }
                  });
               }
            });
         }
      });
   },
   nextPage : function() {
      $.post('api/admin/invites', { page: this.currentPage, size: this.pageSize }, function(data) {
         alertErrors(data.errors);
         invitesTable.append(data.invites);
         if(data.invites.length > 0)
            invitesTable.currentPage++;
         else {
            $('#invites-load-more').removeClass('button-common');
            $('#invites-load-more').addClass('text-regular');
            $('#invites-load-more').html('(end of list)');
            $('#invites-load-more').css({ margin: '0 2em'});
         }
      })
   }
}

var adminStats = {
   template : null,
   init : function() {
      this.template = Tempo.prepare('admin-stats');
   },
   clear : function() {
      this.template.clear();
   },
   render : function(stats) {
      this.template.render(stats);
   }
}

$(document).ready(function() {

   adminStats.init();
   usersTable.init();
   invitesTable.init();

   $.post('api/admin', function(stats) {
      adminStats.render(stats);
   });

});

function disableElem(elem) {
   $('#'+elem).fadeTo(0, 0.75);
   $('#'+elem).data('spin', new Spinner(spinOptions).spin(document.getElementById(elem)));
}

function enableElem(elem) {
   $('#'+elem).fadeTo(0, 1);
   $('#'+elem).data('spin').stop();
}

function alertErrors(errors) {
   if(errors && errors.length > 0) {
      text = "";
      errors.forEach(function(error) {
         text += error + "\n";
      });
      alertify.alert(text);
   }
}

function deleteUser(email) {
   console.log(email);
}