var duration = 0;

function getQueryParams(qs) {
   qs = qs.split("+").join(" ");
   var params = {}, tokens, re = /[?&]?([^=]+)=([^&]*)/g;
   while (tokens = re.exec(qs)) {
      params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
   }
   return params;
}

var spinOptions = {
   lines: 15, // The number of lines to draw
   length: 20, // The length of each line
   width: 5, // The line thickness
   radius: 20, // The radius of the inner circle
   corners: 1, // Corner roundness (0..1)
   rotate: 0, // The rotation offset
   direction: 1, // 1: clockwise, -1: counterclockwise
   color: '#000', // #rgb or #rrggbb or array of colors
   speed: 1, // Rounds per second
   trail: 45, // Afterglow percentage
   shadow: false, // Whether to render a shadow
   hwaccel: false, // Whether to use hardware acceleration
   zIndex: 2e9, // The z-index (defaults to 2000000000)
   top: '50%', // Top position relative to parent
   left: '50%' // Left position relative to parent
}

var locks = {
   processingCredentials : false,
   scroll: false
}

var templates = {
   login: null,
   loggedIn: null
}

var profile = null;

var preloader = {
   spin: null,
   init: function() {
      spin = new Spinner({
      lines: 25,
      length: 35,
      width: 5,
      radius: 60,
      corners: 1,
      rotate: 0,
      direction: 1,
      color: 'white',
      speed: 1,
      trail: 45,
      shadow: false,
      hwaccel: false,
      zIndex: 2e9,
      top: '50%',
      left: '50%'
   });
      spin.spin(document.getElementById('preloader'));
   },
   hide: function(callback) {
      $('#preloader').fadeTo(200, 0);
      setTimeout(function() {
         $('#preloader').hide();
      }, 300);

      spin.stop();
      callback();
   },
   afterload: function() {
      preloader.hide(function() {
         setTimeout(function() {
            $("#play-container").animate({"top": "0"}, 500);
            $("#right-bar").animate({"top": "0"}, 500);
            setTimeout(function() {
               tutorial.start();
            }, 500);
         }, 1);
      });
   }
}

var tutorial = {
   tour: null,
   init: function() {
      this.tour = new Shepherd.Tour({
         defaults: {
           classes: 'shepherd-element shepherd-open shepherd-theme-arrows',
           showCancelLink: false
         }
      });
      this.tour.addStep('hi', {
         title: 'Hello! First time here?',
         text: [
            'WeePlayer allows you to listen music and create your own playlists of audio tracks.',
            'Playlists are stored in our database so you can access them anytime from anywhere'
         ],
         showCancelLink: true
      });
      this.tour.addStep('hi', {
         title: 'Using weeplayer is very easy',
         text: [
            'You just need to SIGN UP with email to rule your audio library',
            'We promise not to send spam :) Email is needed for identification purposes only'
         ],
         attachTo: '#credentials-container'
      });
      this.tour.addStep('hi', {
         text: [
            'Create a playlist',
         ],
         attachTo: '#create-playlist'
      });
      this.tour.addStep('hi', {
         text: [
            'And RIGHT-CLICK on a tracks to add them to it',
         ],
         attachTo: '#track-title-0'
      });
      this.tour.addStep('hi', {
         text: [
            'Did not find favorite song? Then upload it from your computer!',
            'Help our library grow'
         ],
         attachTo: '#button-upload',
         scrollTo: true
      });
      this.tour.addStep('hi', {
         text: [
            'Hotkeys will also serve you well :)',
         ],
         attachTo: '#hotkeys',
         scrollTo: true,
         buttons: [
            {
               text: 'Okay!',
               action: function() {
                  tutorial.tour.complete();
                  if(locks.scroll) return;
                  locks.scroll = true;
                  $("html, body").animate({ scrollTop: 0 }, 200);
                  setTimeout(function() { locks.scroll = false }, 200);
               }
           }
         ]
      });
   },
   start: function() {
      if(!simpleStorage.get('tutorShown'))
         this.tour.start();
      simpleStorage.set('tutorShown', true);
   }
}

var player = {
   player: null,
   isPlaying: false,
   volume: 100,
   mode: null, //repeat, next, random
   src: null,
   trackId: null,
   init: function() {
      this.toggleModeFromStr(simpleStorage.get('playerMode') || 'next');
   },
   play: function() {
      if(!this.player) {
         this.setSrc(this.src);
      }
      if(!this.isPlaying)
         this.player.play();
      this.isPlaying = true;
      $("#favicon").attr("href","favicon_playing.ico");
      $('#play').removeClass('button-play');
      $('#play').addClass('button-pause');
   },
   pause: function() {
      if(!this.player) return;
      if(this.isPlaying)
         this.player.pause();
      this.isPlaying = false;
      $("#favicon").attr("href","favicon_paused.ico");
      $('#play').removeClass('button-pause');
      $('#play').addClass('button-play');
   },
   setSrc: function(src, play) {
      this.src = src;
      if(play === undefined || play === null) play = true;
      if(this.player) {
         this.player.stop();
         this.player.destruct();
      }
      this.player = soundManager.createSound({
         url: src,
         autoLoad: true,
         autoPlay: play,
         whileplaying: function() {
            updateCurrentTime(this.position / this.duration * 100);
            updateDurationText(this.position, this.duration);
         },
         whileloading: function() {
            updateLoadProgress(this.bytesLoaded / this.bytesTotal);
         },
         onfinish: function() {
            if(player.mode == 'repeat')
               this.play();
            if(player.mode == 'random')
               tracksTable.playRandom();
            if(player.mode == 'next') {
               tracksTable.playNext();
            }
         },
         onload: function() {
            //if(play)
               //player.play();
         }
      });
      $('#volume').css({ width: (player.getVolume())+'%' });
      if(!simpleStorage.get('volume')) $('#volume').css({ width: '100%' });
      else { 
         updateVolume(simpleStorage.get('volume') || 100);
         $('#volume').css({ width: simpleStorage.get('volume')*100+'%'});
      }
      duration = this.player.duration;
   },
   setVolume: function(v) {
      this.volume = v * 100;
      if(this.player)
         this.player.setVolume(this.volume);
   },
   getVolume: function() {
      return this.volume;
   },
   setMuted: function(muted) {
      //this.player.muted = muted;
   },
   setCurrentTime: function(time) {
      this.player.setPosition(time * this.player.duration);
   },
   setTitle: function(title) {
      if(title.length > 57)
         title = title.substring(0, 57)+'...';
      $('#track-title').text(title);
   },
   toggleModeFromStr: function(str) {
      if(str == 'repeat') this.toggleRepeat();
      if(str == 'next') this.toggleNext();
      if(str == 'random') this.toggleRandom();
   },
   toggleRepeat: function() {
      if(this.mode == 'repeat') return;
      this.mode = 'repeat';
      $('#toggle-repeat').addClass('player-mode-enabled');
      $('#toggle-next').removeClass('player-mode-enabled');
      $('#toggle-random').removeClass('player-mode-enabled');
      simpleStorage.set('playerMode', this.mode);
   },
   toggleNext: function() {
      if(this.mode == 'next') return;
      this.mode = 'next';
      $('#toggle-repeat').removeClass('player-mode-enabled');
      $('#toggle-next').addClass('player-mode-enabled');
      $('#toggle-random').removeClass('player-mode-enabled');
      simpleStorage.set('playerMode', this.mode);
   },
   toggleRandom: function() {
      if(this.mode == 'random') return;
      this.mode = 'random';
      $('#toggle-repeat').removeClass('player-mode-enabled');
      $('#toggle-next').removeClass('player-mode-enabled');
      $('#toggle-random').addClass('player-mode-enabled');
      simpleStorage.set('playerMode', this.mode);
   }
}

var contextMenus = {
   init: function() {
      $.contextMenu({
         selector: '#template-tracks-list', 
         build: function($trigger, e) {
            var track = tracksTable.data[tracksTable.hoverIndex];
            var trackItems = [
               {
                  name: 'Copy title "'+track.title+'"', 
                  callback: function(key, opt) {
                     tracksTable.copyTitle(tracksTable.hoverIndex);
                  }
               }
            ];
            if(profile.loggedIn && tracksTable.currentPlaylist == null) {
               trackItems.push('---');
               playlists.data.forEach(function(playlist) {
                  trackItems.push({
                     name: 'Add to '+playlist.title,
                     callback: function(key, opt) {
                        playlists.pushTo(playlist.id, track.id);
                        alertify.log('Added ' + track.title + ' to '+playlist.title);
                     }
                  });
               });
            }
            if(profile.loggedIn && tracksTable.currentPlaylist != null) {
               trackItems.push('---');
               trackItems.push({
                  name: 'Delete from '+tracksTable.currentPlaylist.title,
                  callback: function(key, opt) {
                     playlists.deleteFrom(tracksTable.currentPlaylist.id, track.id);
                  }
               });
               playlists.data.forEach(function(playlist) {
                  if(playlist.id != tracksTable.currentPlaylist.id)
                     trackItems.push({
                        name: 'Add to '+playlist.title+ ' too',
                        callback: function(key, opt) {
                           playlists.pushTo(playlist.id, track.id);
                           alertify.log('Added ' + track.title + ' to '+playlist.title);
                        }
                     });
               });
               trackItems.push('---');
               trackItems.push({
                  name: 'Rename this playlist',
                  callback: function(key, opt) {
                     playlists.renamePlaylist(tracksTable.currentPlaylist);
                  }
               });
               trackItems.push({
                  name: 'Delete this playlist (permanently)',
                  callback: function(key, opt) {
                     playlists.deletePlaylist(tracksTable.currentPlaylist.id);
                  }
               });
            }
            if(profile.isAdmin) {
               trackItems.push('--');
               trackItems.push({
                  name: 'Rename', 
                  callback: function(key, opt) {
                     var track = tracksTable.data[tracksTable.hoverIndex];
                     alertify.prompt('Enter new track title', function(e, title) {
                        if(e) {
                           var index = tracksTable.hoverIndex;
                           $.post('api/tracks/rename', { id: track.id, title: title }, function(data) {
                              if(data.errors) {
                                 alertErrors(data.errors);
                              } else {
                                 track.title = title;
                                 $('#track-title-'+index).text(title);
                                 alertify.log('Renamed to ' + title);
                              }
                           });
                        }
                     }, track.title);
                  }
               });
               trackItems.push({
                  name: 'Delete (permanently)', 
                  callback: function(key, opt) {
                     var track = tracksTable.data[tracksTable.hoverIndex];
                     tracksTable.deleteTrack(track.id, tracksTable.hoverIndex);
                  }
               });
            }

            return {
               callback: function(key, options) {},
               items: trackItems
            };
         }
      });
      
   },
   clearTrackMenus: function() {
      //not implemented yet
   }
}

var tracksTable = {
   template : null,
   data : [],
   currentPage : 0,
   pageSize : 100,
   url: 'api/tracks',
   uploading: false,
   currentPlayingId: null,
   currentIndex: 0,
   hoverIndex: 0,
   currentPlaylist: null,
   preInit: function() {
      $('#fileuploader').hide();
      this.template = Tempo.prepare('template-tracks-list');
   },
   init : function() {
      var params = getQueryParams(window.location.search);
      if(params.search) {
         $('#search-tracks').val(params.search.split('+').join(' '));
         this.search(function(data) {
            var _this = tracksTable;
            if(data.length > 0) {
               _this.currentIndex = 0;
               _this.currentPlayingId = _this.data[0].id;
               _this.data[0].playing = true;
               player.setTitle(_this.data[0].title);
               player.trackId = _this.data[0].id;
               player.setSrc('api/track?id='+_this.data[0].id, false);
            }
            preloader.afterload();
         });
      } else {
         this.nextPage(false, function(data) {
            var _this = tracksTable;
            if(data.length > 0) {
               _this.currentIndex = 0;
               _this.currentPlayingId = _this.data[0].id;
               _this.data[0].playing = true;
               player.setTitle(_this.data[0].title);
               player.trackId = _this.data[0].id;
               player.setSrc('api/track?id='+_this.data[0].id, false);
            }
            preloader.afterload();
         });
      }

      Mousetrap.bind('ctrl+right', function(e) {
         tracksTable.playNext();
         return false;
      });
      Mousetrap.bind('ctrl+left', function(e) {
         tracksTable.playPrevious();
         return false;
      });
      Mousetrap.bind(['ctrl+r','ctrl+к'], function(e) {
         tracksTable.playRandom();
         return false;
      });
      Mousetrap.bind('space', function(e) {
         togglePlay();
         return false;
      });
      Mousetrap.bind(['f','а'], function(e) {
         tracksTable.scrollToTrack();
         return false;
      });
      Mousetrap.bind(['w','ц'], function(e) {
         if(locks.scroll) return;
         locks.scroll = true;
         $("html, body").animate({ scrollTop: 0 }, 200);
         setTimeout(function() { locks.scroll = false }, 200);
         return false;
      });
      Mousetrap.bind(['s','ы'], function(e) {
         if(locks.scroll) return;
         locks.scroll = true;
         $("html, body").animate({ scrollTop: $(document).height() }, 200);
         setTimeout(function() { locks.scroll = false }, 200);
         return false;
      });
      Mousetrap.bind(['t','е'], function() {
         themes.setNext();
      });
   },
   clear : function() {
      this.data = [];
      this.template.clear();
   },
   repeatRender : function() {
      this.template.render(this.data);
   },
   append : function(tracks) {
      for(var i = 0; i < tracks.length; i++) {
         tracks[i].playing = false;
         this.data.push(tracks[i]);
      }
      this.template.render(this.data);
   },
   appendOne : function(track) {
      track.playing = false;
      this.data.push(track);
      this.template.render(this.data);
   },
   nextPage : function(resetPlay, callback) {
      if(resetPlay === undefined) resetPlay = true;
      $.post(this.url, { page: this.currentPage, size: this.pageSize, sort: '_id,desc' }, function(tracks) {
         $('#playlist-empty').hide();
         var _this = tracksTable;
         _this.append(tracks.content);
         if(_this.currentPage == 0 && !resetPlay) {
            _this.currentIndex = 0;
            _this.currentPlayingId = _this.data[0].id;
            player.setTitle(_this.data[0].title);
            player.setSrc('api/track?id='+_this.data[0].id, false);
            player.trackId = _this.data[0].id;
         }
         _this.currentPage++;
         if(tracks.content.length == 0) {
            $('#tracks-load-more').removeClass('button-common');
            $('#tracks-load-more').addClass('text-regular');
            $('#tracks-load-more').text('(end of list)');
            $('#tracks-load-more').css({ 'margin-left': '2.5em' });
            setTimeout(function() {
               $('#tracks-load-more').hide();
            }, 1000);
         }
         if(callback) callback(tracks);
         tracksTable.resetTableState();
      });
   },
   togglePlay: function(trackId, tempoId) {
      if(this.currentIndex < this.data.length && this.currentIndex != -1) {
         this.data[this.currentIndex].playing = false;
      }
      if(this.data[tempoId] && this.data[tempoId].id != player.trackId) {
         player.setTitle(this.data[tempoId].title);
         player.setSrc('api/track?id='+trackId, true);
         player.trackId = trackId;
      }
      if(tempoId != this.currentIndex) {
         player.play();
         if(this.currentIndex != -1)
            this.data[this.currentIndex].playing = false;
         this.data[tempoId].playing = true;
         $('#play-track-'+this.currentIndex).addClass('button-play-small').removeClass('button-pause-small');
         $('#play-track-'+tempoId).removeClass('button-play-small').addClass('button-pause-small');
      } else {
         togglePlay();
      }
      this.currentIndex = tempoId;
      this.currentPlayingId = trackId;
   },
   playNext: function() {
      if(this.currentIndex == -1) return;

      var next = this.currentIndex >= (this.data.length-1) ? 0 : this.currentIndex + 1;
      this.data[this.currentIndex].playing = false;
      $('#play-track-'+this.currentIndex).addClass('button-play-small').removeClass('button-pause-small');
      $('#play-track-'+next).removeClass('button-play-small').addClass('button-pause-small');
      this.data[next].playing = true;
      this.currentIndex = next;
      this.currentPlayingId = this.data[next].id;
      player.setTitle(this.data[next].title);
      player.setSrc('api/track?id='+this.data[next].id);
      player.play();
      this.scrollToTrack();
   },
   playPrevious: function() {
      if(this.currentIndex == -1) return;

      var next = this.currentIndex == 0 ? this.data.length-1 : this.currentIndex - 1;
      this.data[this.currentIndex].playing = false;
      $('#play-track-'+this.currentIndex).addClass('button-play-small').removeClass('button-pause-small');
      $('#play-track-'+next).removeClass('button-play-small').addClass('button-pause-small');
      this.data[next].playing = true;
      this.currentIndex = next;
      this.currentPlayingId = this.data[next].id;
      player.setTitle(this.data[next].title);
      player.setSrc('api/track?id='+this.data[next].id);
      player.play();
      this.scrollToTrack();
   },
   playSame: function() {
      player.play();
   },
   playRandom: function() {
      var rnd = getRandomArbitary(0, this.data.length);
      if(this.currentIndex != -1 && this.currentIndex < this.data.length)
         this.data[this.currentIndex].playing = false;
      $('#play-track-'+this.currentIndex).addClass('button-play-small').removeClass('button-pause-small');
      $('#play-track-'+rnd).removeClass('button-play-small').addClass('button-pause-small');
      this.data[rnd].playing = true;
      this.currentIndex = rnd;
      this.currentPlayingId = this.data[rnd].id;
      player.setTitle(this.data[rnd].title);
      player.setSrc('api/track?id='+this.data[rnd].id);
      player.trackId = this.data[rnd].id;
      player.play();
      this.scrollToTrack();
   },
   uploadNew : function() {
      if(!profile || !profile.loggedIn) {
         alertify.alert('Please sign in');
         return;
      }
      if(this.uploading) {
         $('#upload').hide();
         this.uploading = false;
         $('#button-upload').html('Upload my track');
      } else {
         $('#upload').show();
         this.uploading = true;
         $('#button-upload').html('Hide');
         window.scrollTo(0,document.body.scrollHeight);
      }
   },
   search: function(callback) {
      var title = $('#search-tracks').val();
      title = title.split('+').join(' ');
      window.history.pushState('page2', 'Title', $.query.set("search", title));
      $.post('api/tracks/search', { title: title, page: 0, size: this.pageSize, sort: '_id,asc' }, function(data) {
         $('#playlist-empty').hide();
         var _this = tracksTable;
         _this.currentPlaylist = null;
         _this.currentIndex = 0;
         _this.currentPage = 0;
         _this.clear();

         for(var i = 0; i < data.length; i++) {
            data[i].playing = _this.currentPlayingId == data[i].id;
            _this.data.push(data[i]);
         }

         _this.currentIndex = -1;

         _this.template.render(_this.data);
         if(callback) callback(_this.data);
         tracksTable.resetTableState();
      });
   },
   deleteTrack: function(trackId, tableIndex) {
      alertify.confirm('Really delete '+this.data[tableIndex].title+'?', function(e) {
         if(e) {
            $.post('api/tracks/delete', { id: trackId }, function(data) {
               alertErrors(data.errors);
               if(!(data.errors && data.errors.length > 0)) {
                  $('#track-title-'+tableIndex).text('(deleted)');
               }
            });
            tracksTable.resetTableState();
         }
      });
   },
   copyTitle: function(tableIndex) {
      var title = this.data[tableIndex].title;
      alertify.prompt("Use Ctrl + C",function(){},title);
   },
   resetTableState: function() {
      setTimeout(function() {
         var _this = tracksTable;
         for(var i = 0; i < _this.data.length; i++) {
            if(_this.data[i].id == player.trackId) {
               _this.currentPlayingId = player.trackId;
               _this.currentIndex = i;
               if(player.isPlaying)
                  $('#play-track-'+_this.currentIndex).addClass('button-pause-small').removeClass('button-play-small');
               return;
            }
         }
         _this.currentPlayingId = null;
         _this.currentIndex = -1;
      }, 1);
   },
   scrollToTrack: function() {
      if(locks.scroll) return;
      locks.scroll = true;
      setTimeout(function() { locks.scroll = false }, 100);
      if(this.currentIndex < this.data.length || this.currentIndex != -1)
         $("html, body").animate({
                 scrollTop: $("#track-title-"+tracksTable.currentIndex).offset().top - 70 
             }, 100);
   }
}

var playlists = {
   template: null,
   data: [],
   init: function() {
      this.template = Tempo.prepare('playlists-list');
      this.update();
      $('#playlist-empty').hide();
   },
   update: function() {
      $.post('api/playlists', {}, function(data) {
         var _this = playlists;
         _this.data = data;
         _this.template.render(_this.data);
      });
   },
   loadPlaylist: function(id) {
      $.post('api/playlist', { id: id }, function(playlist) {
         tracksTable.currentPlaylist = playlist;
         tracksTable.data = playlist.tracks || [];
         if(tracksTable.data.length == 0) {
            $('#playlist-empty').show();
         } else {
            $('#playlist-empty').hide();
         }
         tracksTable.template.render(tracksTable.data.reverse());
         tracksTable.resetTableState();
      });
   },
   loadMostRecent: function() {
      tracksTable.currentPage = 0;
      tracksTable.currentPlaylist = null;
      tracksTable.data = [];
      tracksTable.search("");
      tracksTable.resetTableState();
   },
   create: function() {
      if(!profile || !profile.loggedIn) {
         alertify.alert('Please sign in');
         return;
      }
      alertify.prompt('Enter playlist name', function(e, title) {
         if(e) {
            if(title.length == 0) {
               alertify.alert('Playlist name cannot be empty');
               return;
            } 
            if(title.length > 50) {
               alertify.alert('Playlist name is too long');
               return;
            }
            $.post('api/playlists/create', { title: title }, function(playlist) {
               alertErrors(playlist.errors);
               if(!playlist.errors || playlist.errors.length == 0) {
                  var _this = playlists;
                  if(!_this.data) _this.data = [];
                  _this.data.push(playlist);
                  _this.template.render(_this.data);
               }
            });
         }
      }, 'New playlist');
   },
   deletePlaylist: function(playlistId) {
      alertify.confirm('Really delete playlist "'+tracksTable.currentPlaylist.title+'"?', function(e) {
         if(e) {
            $.post('api/playlists/delete', { id: playlistId }, function(data) {
               console.log(data);
               playlists.update();
            });
         }
      });
   },
   pushTo: function (playlistId, trackId) {
      $.post('api/playlist/pushTo', { playlistId: playlistId, trackId: trackId }, function(data) {
         alertErrors(data.errors);
      });
   },
   deleteFrom: function(playlistId, trackId) {
      $('#track-title-'+tracksTable.hoverIndex).text('(deleted from playlist)');
      $.post('api/playlist/deleteFrom', { playlistId: playlistId, trackId: trackId }, function(data) {
         alertErrors(data.errors);
      });
   },
   renamePlaylist: function(playlist) {
      alertify.prompt('Enter new playlist title', function(e, title) {
         if(!e) return;
         $.post('api/playlists/rename', { id: playlist.id, title: title }, function(data) {
            alertErrors(data.errors);
            if(!data.errors || data.errors.length == 0) {
               playlists.update();
            }
         });
      }, playlist.title);
   }
}

var themes = {
   themes: ['default', 'css/theme-blue.css', 'css/theme-purple.css', 'css/theme-green.css'],
   current: 0,
   init: function() {
      var current = simpleStorage.get('theme') || 0;
      this.setTheme(this.themes[current]);
      this.current = current;
   },
   setNext: function() {
      var next = this.current + 1;
      if(next >= this.themes.length) next = 0;
      $('#css-theme').remove();
      if(this.themes[next] != 'default') {
         $('head').append('<link id="css-theme" rel="stylesheet" href="'+this.themes[next]+'" type="text/css"/>');
      }
      this.current = next;
      simpleStorage.set('theme', this.current);
   },
   setTheme: function(theme) {
      $('#css-theme').remove();
      if(theme != 'default') {
         $('head').append('<link id="css-theme" rel="stylesheet" href="'+theme+'" type="text/css"/>');
      }
   }
}

$(document).ready(function() {
   preloader.init();
   tutorial.init();

   templates.login = Tempo.prepare('login');
   templates.loggedIn = Tempo.prepare('loggedIn');

   tracksTable.preInit();
   soundManager.flashLoadTimeout = 0;
   soundManager.setup({
      url: 'soundmanager2/swf',
      preferFlash: true,
      onready: function() {
         player.init();
         themes.init();
         tracksTable.init();
      }
   });

   $('#content').hide();

   disableElem('content');
   if(simpleStorage.get('email') != '') {
      $.post('api/profile/signin', { email: simpleStorage.get('email'), password: simpleStorage.get('pw') }, function(profile) {
         updateView_profile(profile);
         playlists.init();
         $('#content').show();
         enableElem('content');
         contextMenus.init();
      });
   } else {
      $.post('api/profile', function(profile) {
         updateView_profile(profile);
         playlists.init();
         $('#content').show();
         enableElem('content');
         contextMenus.init();
      });
   }
   
   //player.setVolume(simpleStorage.get('volume') || 100);
   updateCurrentTime(0);

   $('#progress-container').click(function(e) {
      seekPosition((e.offsetX || e.clientX - $(e.target).offset().left));
   });

   $('#volume-container').mousemove(function(e) {
      if(!window.movingVolume) return;
      var offsetX  = (e.offsetX || e.clientX - $(e.target).offset().left);
      var value = offsetX / $('#volume-container').width();
      updateVolume(value);
      if(player.getVolume() > 100) player.setVolume(100);
      $('#volume').css({ width: (player.getVolume())+'%' });
   });
   $('#volume-container').mousedown(function(e) {
      window.movingVolume = true;
      var offsetX  = (e.offsetX || e.clientX - $(e.target).offset().left);
      var value = offsetX / $('#volume-container').width();
      updateVolume(value);
      if(player.getVolume() > 100) player.setVolume(100);
      $('#volume').css({ width: (player.getVolume())+'%' });
   });

   $(document).mouseup(function(e) {
      window.movingVolume = false;
   });

   /*$('#volume-container').mousemove(function(e) {
      if(e.which == 1) {
         var value = e.offsetX / $('#volume-container').width();
         updateVolume(value);
         $('#volume').css({ width: (value*100)+'%' });
      }
   });*/
   var filename = null;
   $('#fileuploader').uploadFile({
      url: 'api/tracks/upload',
      allowedTypes: 'mp3',
      acceptFiles: 'audio/',
      multiple: false,
      dynamicFormData: function() {
         var data = {
            "title" : filename.substring(0, filename.length-4)
         };
         return data;
      },
      onSelect: function(files) {
         filename = files[0].name;
         files[0].size;
         return true;
      },
      onSuccess: function(files, data) {
         alertErrors(data.errors);
         if(data.track) {
            tracksTable.data.push(data.track);
            tracksTable.template.render(tracksTable.data);
            alertify.log('Uploaded ' + data.track.title);
            if(tracksTable.currentPlaylist != null) {
               playlists.pushTo(tracksTable.currentPlaylist.id, data.track.id);
               alertify.log('Added '+data.track.title + ' to ' + tracksTable.currentPlaylist.title);
            }
         }
      }
   });

   $('#upload').hide();
});

function togglePlay() {
   if(player.isPlaying) {
      player.pause();
      if(tracksTable.currentIndex == -1) return;
      $('#play-track-'+tracksTable.currentIndex).addClass('button-play-small').removeClass('button-pause-small');
   } else {
      player.play();
      if(tracksTable.currentIndex == -1) return;
      $('#play-track-'+tracksTable.currentIndex).removeClass('button-play-small').addClass('button-pause-small');
   }
}

function updateCurrentTime(percent) {
   $('#currentTime').css({ width: percent + '%' });
}

function updateVolume(value) {
   if(value > 1) value = 1;
   if(value <= 0) {
      value = 0;
      player.setMuted(true);
   } else player.setMuted(false);
   player.setVolume(value);
   $('#volume-text').html(Math.round((value * 100)) + '%');
   simpleStorage.set('volume', value);
}

function updateLoadProgress(ratio) {
   if(ratio != 0 && ratio != 1)
      $('#currentLoad').css({ width: ratio*100+'%' });
   else 
      $('#currentLoad').css({ width: '100%' });
}

function seekPosition(offsetX) {
   var totalWidth = $('#progress-container').width();
   player.setCurrentTime((offsetX / totalWidth));
}

function updateDurationText(currentTime, totalTime) {
   $('#duration-text').html(formatTime(currentTime) + ' / ' + formatTime(totalTime));
}

function formatTime(seconds) {
   var date = new Date(1970,0,1);
   date.setSeconds(seconds / 1000);
   return date.toTimeString().replace(/.*(\d{2}:\d{2}).*/, "$1");
}

function disableElem(elem) {
   //$(document.body).data('spin', new Spinner(spinOptions).spin(document.getElementById('#content')));
}

function enableElem(elem) {
   //$(document.body).data('spin').stop();
}

function tryLogin(email, pw) {
   if(!email || !pw) {
      var email = $('#email').val();
      var pw = $('#pw').val();
   }
   var remember = $('#rememberLogin').prop('checked');
   if(locks.processingCredentials) return;

   locks.processingCredentials = true;
   disableElem('content');
   $.post("api/profile/signin", { email: email, password: pw }, function(response) {
      updateView_profile(response);
      playlists.update();
      enableElem('content');
      locks.processingCredentials = false;
      alertErrors(response.errors);
      if(response.errors.length == 0 && remember) {
         simpleStorage.set('email', email);
         simpleStorage.set('pw', pw);
      } else {
         simpleStorage.set('email', '');
         simpleStorage.set('pw', '');
      }
      contextMenus.init();
   });
}

function signup() {
   var email = $('#email').val();
   var pw = $('#pw').val();
   var remember = $('#rememberLogin').prop('checked');
   if(locks.processingCredentials) return;
   locks.processingCredentials = true;
   disableElem('content');

   alertify.prompt("Enter code phrase", function (e, codePhrase) {
       if (e) {
           $.post("api/profile/signup", { email: email, password: pw, codePhrase: codePhrase }, function(response) {
              updateView_profile(response);
              playlists.update();
              enableElem('content');
              locks.processingCredentials = false;
              alertErrors(response.errors);
              if(response.errors.length == 0 && remember) {
                 simpleStorage.set('email', email);
                 simpleStorage.set('pw', pw);
              } else {
                 simpleStorage.set('email', '');
                 simpleStorage.set('pw', '');
              }
           });
       } else {
           enableElem('content');
           locks.processingCredentials = false;
       }
   }, "");
}

function logout() {
   if(locks.processingCredentials) return;

   simpleStorage.set('email', '');
   simpleStorage.set('pw', '');
   disableElem('content');
   locks.processingCredentials = true;
   $.post("api/profile/signout", function(response) {
      playlists.update();
      updateView_profile(response);
      enableElem('content');
      locks.processingCredentials = false;
      alertErrors(response.errors);
      contextMenus.init();
   });
}

function setPassword() {
   if(locks.processingCredentials) return;

   locks.processingCredentials = true;
   disableElem('content');

   alertify.secret("Enter new password", function (e, pw) {
      if (e) {
         $.post("api/profile/setpw", { password: pw }, function(response) {
            updateView_profile(response);
            enableElem('content');
            locks.processingCredentials = false;
            alertErrors(response.errors);
            if(response.errors.length == 0 && simpleStorage.get('pw') != '') {
               simpleStorage.set('pw', pw);
           }
         });
      } else {
         enableElem('content');
         locks.processingCredentials = false;
         return;
      }
   }, "");
}

function setEmail() {
   if(locks.processingCredentials) return;

   locks.processingCredentials = true;
   disableElem('content');

   alertify.prompt("Enter new e-mail", function (e, email) {
      if (e) {
         $.post("api/profile/setemail", { email: email }, function(response) {
            updateView_profile(response);
            enableElem('content');
            locks.processingCredentials = false;
            alertErrors(response.errors);
            if(response.errors.length == 0 && simpleStorage.get('email') != '') {
               simpleStorage.set('email', email);
            }
         });
      } else {
         enableElem('content');
         locks.processingCredentials = false;
         return;
      }
   }, "");
}

function updateView_profile(profile) {
   window.profile = profile;
   profile.logged = profile.loggedIn;
   profile.notlogged = !profile.loggedIn;
   profile.isadmin = profile.isAdmin;
   if(profile.email && profile.email.length > 18) {
      profile.email = profile.email.substring(0, 18)+'...';
   }
   templates.login.render(profile);
   templates.loggedIn.render(profile);
   if(!profile.isAdmin) {
      $('#admin-button').hide();
   }
}

function adminPage() {
   window.open('admin.html', '_blank').focus();
}

function alertErrors(errors) {
   if(errors && errors.length > 0) {
      text = "";
      errors.forEach(function(error) {
         text += error + "\n";
      });
      alertify.alert(text);
   }
}

function getRandomArbitary(min, max) {
   return Math.floor((Math.random() * max) + min);
}