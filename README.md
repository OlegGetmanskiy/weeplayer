WeePlayer - это сервис прослушивания и синхронизации музыки. Позволяет загружать свои аудио файлы, составлять списки воспроизведения, прослушивать музыку в браузере.

Есть desktop-клиент: https://bitbucket.org/OlegusGetman/weeplayer-desktop

Разработчик: Олег Гетманский. 
Со мной можно связаться по e-mail: guard@olegusgetman.ru

http://weemusic.tk/

![alt tag](http://i.imgur.com/9Kl9jCD.png)
![alt tag](http://i.imgur.com/eC92PEz.png)
![alt tag](http://i.imgur.com/gxSiYMG.png)
![alt tag](http://i.imgur.com/ExGoanb.png)