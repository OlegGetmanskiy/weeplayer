------- PROFILE -------

/api/profile (done)
params: (none)
response: {
   loggedIn: (boolean)
   isAdmin: (boolean)
   email: (string)
   errors: (string[])
}

/api/profile/signup (done)
params: email (string), password (string), codePhrase (string)
response: {
   loggedIn: (boolean)
   isAdmin: (boolean)
   email: (string)
   errors: (string[])
}

/api/profile/signin (done)
params: email (string), password (string)
response: {
   loggedIn: (boolean)
   isAdmin: (boolean)
   email: (string)
   errors: (string[])
}

/api/profile/signout (done)
params: (none)
response: {
   loggedIn: (boolean)
   isAdmin: (boolean)
   email: (string)
   errors: (string[])
}

/api/profile/setpw (done)
params: password (string)
response: {
   loggedIn: (boolean)
   isAdmin: (boolean)
   email: (string)
   errors: (string[])
}

/api/profile/setemail (done)
params: email (string)
response: {
   loggedIn: (boolean)
   isAdmin: (boolean)
   email: (string)
   errors: (string[])
}

------ ADMIN ------

/api/admin (done)
params: (none)
response: {
   totalTracks: (long)
   totalSpace: (long)
   totalUsers: (long)
   errors: (string[])
}

/api/admin/users (done)
params: page (long), size (long)
response: {
   users: {
         email: (string)
         isAdmin: (boolean)
      }[]
   errors: (string[])
}

/api/admin/users/remove (done)
params: email (string)
response: {
   errors: (string[])
}

/api/admin/invites (done)
params: page (long), size (long)
response: {
   invites: {
      email: (string)
      code: (string)
   }[]
   errors: (string[])
}

/api/admin/invites/add (done)
params: email (string), code (string)
response: {
   errors: (string[])
}

/api/admin/invites/remove (done)
params: email (string)
response: {
   errors: (string[])
}