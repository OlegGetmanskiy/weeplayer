/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.web.response;

import java.util.List;
import org.getmansky.domain.invite.EmailInvite;

/**
 *
 * @author OlegusGetman
 */
public class InvitesListResponse implements PossibleErrorsContainer {
   private List<String> errors;
   private List<EmailInvite> invites;

   @Override
   public List<String> getErrors() {
      return errors;
   }

   @Override
   public void setErrors(List<String> errors) {
      this.errors = errors;
   }

   public List<EmailInvite> getInvites() {
      return invites;
   }

   public void setInvites(List<EmailInvite> invites) {
      this.invites = invites;
   }
   
   
}
