/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.web.response;

import java.util.List;

/**
 *
 * @author OlegusGetman
 */
public class StatsResponse implements PossibleErrorsContainer {

   private List<String> errors;
   private Long totalTracks = 0L;
   private Long totalSpace = 0L;
   private Long totalUsers = 0L;

   public Long getTotalTracks() {
      return totalTracks;
   }

   public void setTotalTracks(Long totalTracks) {
      this.totalTracks = totalTracks;
   }

   public Long getTotalSpace() {
      return totalSpace;
   }

   public void setTotalSpace(Long totalSpace) {
      this.totalSpace = totalSpace;
   }

   public Long getTotalUsers() {
      return totalUsers;
   }

   public void setTotalUsers(Long totalUsers) {
      this.totalUsers = totalUsers;
   }
   
   public void setErrors(List<String> errors) {
      this.errors = errors;
   }
   
   @Override
   public List<String> getErrors() {
      return errors;
   }
   
}
