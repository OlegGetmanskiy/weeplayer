/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.web.response;

import java.util.List;

/**
 *
 * @author OlegusGetman
 */
public class UsersListResponse implements PossibleErrorsContainer {
   
   private List<String> errors;
   private List<User> users;
   
   public static class User {
      private String email;
      private boolean isAdmin;

      public User(String email, boolean isAdmin) {
         this.email = email;
         this.isAdmin = isAdmin;
      }
      
      public String getEmail() {
         return email;
      }
      public void setEmail(String email) {
         this.email = email;
      }
      public boolean isIsAdmin() {
         return isAdmin;
      }

      public void setIsAdmin(boolean isAdmin) {
         this.isAdmin = isAdmin;
      }
   }

   public List<User> getUsers() {
      return users;
   }

   public void setUsers(List<User> users) {
      this.users = users;
   }

   @Override
   public void setErrors(List<String> errors) {
      this.errors = errors;
   }

   @Override
   public List<String> getErrors() {
      return errors;
   }
   
}
