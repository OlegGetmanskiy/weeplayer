/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.web.response;

import java.util.ArrayList;
import java.util.List;
import org.getmansky.domain.audio.Track;
import org.getmansky.domain.playlist.Playlist;

/**
 *
 * @author OlegusGetman
 */
public class PlaylistResponse implements PossibleErrorsContainer {
   private List<String> errors;
   private String id;
   private String title;
   private List<Track> tracks;

   public static List<PlaylistResponse> fromManyPlaylists(List<Playlist> pls) {
      List<PlaylistResponse> r = new ArrayList();
      for(Playlist pl : pls) {
         PlaylistResponse plr = new PlaylistResponse();
         plr.setId(pl.getId());
         plr.setTitle(pl.getTitle());
         r.add(plr);
      }
      return r;
   }
   
   public static PlaylistResponse fromPlayList(Playlist pl) {
      PlaylistResponse r = new PlaylistResponse();
      r.setId(pl.getId());
      r.setTitle(pl.getTitle());
      r.setTracks(pl.getTracks());
      return r;
   }
   
   @Override
   public List<String> getErrors() {
      return errors;
   }

   @Override
   public void setErrors(List<String> errors) {
      this.errors = errors;
   }

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public List<Track> getTracks() {
      return tracks;
   }

   public void setTracks(List<Track> tracks) {
      this.tracks = tracks;
   }
}
