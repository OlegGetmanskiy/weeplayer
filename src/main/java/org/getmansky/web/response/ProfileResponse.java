/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.web.response;

import java.util.List;
import org.getmansky.domain.profile.Profile;

/**
 *
 * @author OlegusGetman
 */
public class ProfileResponse implements PossibleErrorsContainer {
   
   private List<String> errors;
   private Boolean loggedIn = false;
   private Boolean isAdmin = false;
   private String email;

   public ProfileResponse() { }
   
   public ProfileResponse(String email, List<String> errors) {
      this.errors = errors;
      this.email = email;
   }
   
   public ProfileResponse fromProfile(Profile p) {
      ProfileResponse pr = new ProfileResponse();
      pr.setEmail(p.getEmail());
      pr.setIsAdmin(p.getIsAdmin());
      return pr;
   }

   public void setErrors(List<String> errors) {
      this.errors = errors;
   }
   
   @Override
   public List<String> getErrors() {
      return errors;
   }

   public Boolean getLoggedIn() {
      return loggedIn;
   }

   public void setLoggedIn(Boolean loggedIn) {
      this.loggedIn = loggedIn;
   }

   public Boolean getIsAdmin() {
      return isAdmin;
   }

   public void setIsAdmin(Boolean isAdmin) {
      this.isAdmin = isAdmin;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }
}
