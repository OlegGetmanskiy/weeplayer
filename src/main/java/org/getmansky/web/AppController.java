/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.web;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.PatternSyntaxException;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.getmansky.domain.audio.Track;
import org.getmansky.domain.invite.EmailInvite;
import org.getmansky.domain.playlist.Playlist;
import org.getmansky.domain.profile.Profile;
import org.getmansky.repo.FileStorage;
import org.getmansky.repo.InviteRepository;
import org.getmansky.repo.PlaylistRepository;
import org.getmansky.repo.ProfileRepository;
import org.getmansky.repo.TrackRepository;
import org.getmansky.util.EmailValidator;
import org.getmansky.util.MD5;
import org.getmansky.web.response.EmptyResponse;
import org.getmansky.web.response.InvitesListResponse;
import org.getmansky.web.response.PlaylistResponse;
import org.getmansky.web.response.PossibleErrorsContainer;
import org.getmansky.web.response.ProfileResponse;
import org.getmansky.web.response.StatsResponse;
import org.getmansky.web.response.TrackResponse;
import org.getmansky.web.response.UsersListResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author OlegusGetman
 */
@RestController
@Scope("session")
public class AppController implements Serializable {
   
   @Autowired
   private ProfileRepository profileRepo;
   @Autowired
   private InviteRepository inviteRepo;
   @Autowired
   private TrackRepository trackRepo;
   @Autowired
   private PlaylistRepository playlistRepo;

   @Autowired
   private MongoOperations mongoOps;
   
   @Autowired
   private FileStorage fileStorage;
   
   private Profile loggedInProfile = null;
   
   private final static Map<String, Playlist> cachedPlaylists = new HashMap();
   
   @Value("${max.tracks}")
   private Integer maxTracks;
   
   @RequestMapping(value = "/api/profile")
   public ProfileResponse profile() {
      ProfileResponse pr = new ProfileResponse();
      if(loggedInProfile != null) {
         pr.setLoggedIn(Boolean.TRUE);
         pr.setIsAdmin(loggedInProfile.getIsAdmin());
         pr.setEmail(loggedInProfile.getEmail());
      } else {
         pr.setLoggedIn(Boolean.FALSE);
      }
      return pr;
   }
   
   @RequestMapping(value = "/api/profile/signin")
   public ProfileResponse signin(
           @RequestParam(defaultValue = "")String email, 
           @RequestParam(defaultValue = "")String password) {
      
      ProfileResponse r = new ProfileResponse();
      List<String> errors = new ArrayList();
      Profile p;
      if((p = profileRepo.findByEmail(email)) != null) {
         if(MD5.md5Equals(password, p.getEncryptedPassword())) {
            loggedInProfile = p;
            r.setEmail(p.getEmail());
            r.setIsAdmin(p.getIsAdmin());
            r.setLoggedIn(Boolean.TRUE);
         } else {
            loggedInProfile = null;
            errors.add("Incorrect email or password");
         }
      } else {
         loggedInProfile = null;
         errors.add("Incorrect email or password");
      }
      r.setErrors(errors);
      return r;
   }
   
   @RequestMapping(value = "api/profile/signup")
   public ProfileResponse signup(String email, String password, String codePhrase) {
      ProfileResponse r = new ProfileResponse();
      List<String> errors = new ArrayList();
      
      EmailInvite emailInvite = inviteRepo.findByEmailAndCodePhrase(email, codePhrase);
      if(emailInvite == null) {
         errors.add("Sorry, but you're not invited");
         r.setErrors(errors);
         return r;
      } else {
         if(!EmailValidator.isValidEmail(email)) {
            errors.add("This email is not valid");
            r.setErrors(errors);
            return r;
         }
         inviteRepo.deleteByEmail(emailInvite.getEmail());
         Profile p = new Profile();
         p.setEmail(email);
         p.setEncryptedPassword(MD5.md5WithSalt(password));
         p.setIsAdmin(Boolean.FALSE);
         p = profileRepo.save(p);
         
         r.setEmail(email);
         r.setIsAdmin(Boolean.FALSE);
         r.setLoggedIn(Boolean.TRUE);
         loggedInProfile = p;
         return r;
      }
   }
   
   @RequestMapping(value = "/api/profile/setpw")
   public ProfileResponse setPw(
           @RequestParam(defaultValue = "") String password) {

      ProfileResponse r = new ProfileResponse();
      List<String> errors = new ArrayList();
      
      if(loggedInProfile != null && profileRepo.exists(loggedInProfile.getId())) {
         r.setEmail(loggedInProfile.getEmail());
         r.setIsAdmin(loggedInProfile.getIsAdmin());
         r.setLoggedIn(Boolean.TRUE);
         if(!password.isEmpty())
            loggedInProfile.setEncryptedPassword(MD5.md5WithSalt(password));
         else
            errors.add("Your password can not be empty");
         profileRepo.save(loggedInProfile);
      } else {
         errors.add("Not logged in");
      }
      
      r.setErrors(errors);
      return r;
   }
   
   @RequestMapping(value = "/api/profile/setemail")
   public ProfileResponse setEmail(
           @RequestParam(defaultValue = "") String email) {

      ProfileResponse r = new ProfileResponse();
      List<String> errors = new ArrayList();

      if (loggedInProfile != null && profileRepo.exists(loggedInProfile.getId())) {
         r.setIsAdmin(loggedInProfile.getIsAdmin());
         r.setLoggedIn(Boolean.TRUE);
         if(EmailValidator.isValidEmail(email)) {
            loggedInProfile.setEmail(email);
         } else {
            errors.add("This email is not valid");
         }
         r.setEmail(loggedInProfile.getEmail());
         try {
            profileRepo.save(loggedInProfile);
         } catch(Exception e) {
            errors.add("This email is already exists. Please set another email");
         }
      } else {
         errors.add("Not logged in");
      }

      r.setErrors(errors);
      return r;
   }
   
   @RequestMapping(value = "/api/profile/signout")
   public ProfileResponse signout(
           @RequestParam(defaultValue = "") String email) {
      
      loggedInProfile = null;
      ProfileResponse r = new ProfileResponse();
      return r;
   }
   
   private boolean notAdmin() {
      return loggedInProfile == null || !loggedInProfile.getIsAdmin();
   }
   
   private void setForbidden(PossibleErrorsContainer c) {
      List<String> errors = new ArrayList();
      errors.add("Forbidden");
      c.setErrors(errors);
   }
   
   @RequestMapping(value = "/api/admin")
   public StatsResponse stats() {
      StatsResponse r = new StatsResponse();
      if(notAdmin()) {
         setForbidden(r);
         return r;
      }

      r.setTotalUsers(profileRepo.count());
      r.setTotalTracks(trackRepo.count());
      
      return r;
   }
   
   @RequestMapping(value = "/api/admin/users")
   public UsersListResponse users(Pageable page) {
      UsersListResponse r = new UsersListResponse();
      if (notAdmin()) {
         setForbidden(r);
         return r;
      }
      Page<Profile> profiles = profileRepo.findAll(page);
      List<UsersListResponse.User> users = new ArrayList();
      for(Profile p : profiles) {
         users.add(new UsersListResponse.User(p.getEmail(), p.getIsAdmin()));
      }
      
      r.setUsers(users);
      return r;
   }
   
   @RequestMapping(value = "/api/admin/users/remove")
   public EmptyResponse removeUser(String email) {
      EmptyResponse r = new EmptyResponse();
      if (notAdmin()) {
         setForbidden(r);
         return r;
      }
      profileRepo.removeByEmail(email);
      return r;
   }
   
   @RequestMapping(value = "/api/admin/invites")
   public InvitesListResponse invites(Pageable p) {
      InvitesListResponse r = new InvitesListResponse();
      if (notAdmin()) {
         setForbidden(r);
         return r;
      }
      List<EmailInvite> invitesList = new ArrayList();
      Page<EmailInvite> invites = inviteRepo.findAll(p);
      for(EmailInvite i : invites) {
         invitesList.add(i);
      }
      r.setInvites(invitesList);
      return r;
   }
   
   @RequestMapping(value = "/api/admin/invites/add")
   public EmptyResponse addInvite(String email, String codePhrase) {
      EmptyResponse r = new EmptyResponse();
      List<String> errors = new ArrayList();
      if (notAdmin()) {
         setForbidden(r);
         return r;
      }
      
      if(!EmailValidator.isValidEmail(email)) {
         errors.add("This email is not valid");
         r.setErrors(errors);
         return r;
      }
      
      EmailInvite invite = new EmailInvite();
      invite.setEmail(email);
      invite.setCodePhrase(codePhrase);
      inviteRepo.save(invite);
      
      return r;
   }
   
   @RequestMapping(value = "/api/admin/invites/remove")
   public EmptyResponse deleteInvite(String email) {
      EmptyResponse r = new EmptyResponse();
      if (notAdmin()) {
         setForbidden(r);
         return r;
      }
      inviteRepo.deleteByEmail(email);
      return r;
   }
   
   @RequestMapping(value = "/api/tracks/upload")
   public TrackResponse uploadTrack(MultipartFile file, String title, Float duration) {
      TrackResponse r = new TrackResponse();
      if (loggedInProfile == null || !profileRepo.exists(loggedInProfile.getId())) {
         setForbidden(r);
         return r;
      }
      if(trackRepo.count() >= maxTracks) {
         List<String> errs = new ArrayList();
         errs.add("Reached maxmimum amount of tracks, sorry");
         r.setErrors(errs);
         return r;
      }
      if(trackRepo.findByTitle(title) != null) {
         List<String> errs = new ArrayList();
         errs.add("Dublicate track");
         r.setErrors(errs);
         return r;
      }
      try {
         
         if (title == null || title.length() < 3) {
            List<String> errs = new ArrayList();
            errs.add("Title is too short");
            r.setErrors(errs);
            return r;
         }
         if (title.length() > 70) {
            List<String> errs = new ArrayList();
            errs.add("Title is too long. Must be less than 70 characters");
            r.setErrors(errs);
            return r;
         }
         
         ByteArrayOutputStream bas = new ByteArrayOutputStream();
         IOUtils.copy(file.getInputStream(), bas);
         
         Tika tika = new Tika();
         InputStream checkStream = new ByteArrayInputStream(bas.toByteArray());
         String fileType = tika.detect(checkStream);
         
         if(!fileType.equals("audio/mpeg")) {
            List<String> errs = new ArrayList();
            errs.add("Not an mp3 file");
            r.setErrors(errs);
            return r;
         }
         
         InputStream saveStream = new ByteArrayInputStream(bas.toByteArray());
         String id = fileStorage.saveStream(saveStream, file.getName(), "audio/mpeg");
         Track t = new Track();
         t.setFileId(id);
         t.setTitle(title);
         t.setDuration(duration);
         t = trackRepo.save(t);
         r.setTrack(t);
      } catch(IOException ex) {
         List<String> errs = new ArrayList();
         errs.add("Internal server error. Sorry");
         r.setErrors(errs);
         Logger.getLogger(this.getClass()).log(Level.ERROR, ex);
      }
      
      return r;
   }
   
   @RequestMapping(value = "/api/track")
   public void track(
           HttpServletResponse response, 
           @RequestParam(required = true) String id) throws IOException {
      Track track = trackRepo.findById(id);
      if(track == null) {
         response.setStatus(404);
         response.flushBuffer();
         return;
      }
      FileStorage.File file = fileStorage.findFileById(track.getFileId());
      response.setHeader("Content-Type", "audio/mpeg");
      response.setHeader("Content-Length", String.valueOf(file.contentLength));
      response.setHeader("Last-Modified", "Fri, 01 Jan 2010 00:00:01 GMT");
      IOUtils.copy(file.inputStream, response.getOutputStream());
      response.flushBuffer();
   }
   
   @RequestMapping(value = "/api/tracks")
   public Page<Track> allTracks(Pageable page) {
      Page<Track> tracks = trackRepo.findAll(page);
      for(Track t : tracks) {
         t.setFileId(null);
      }
      return tracks;
   }
   
   @RequestMapping(value = "/api/tracks/search")
   public List<Track> searchTracks(Pageable page, String title) {
      Query query = new Query();
      query.skip(page.getOffset());
      query.limit(page.getPageSize());
      try {
         query.addCriteria(Criteria.where("title").regex(title, "i"));
         query = query.with(new Sort(Sort.Direction.DESC, "_id"));
      } catch(PatternSyntaxException ex) {
         return Collections.EMPTY_LIST;
      }
      
      return (List<Track>) mongoOps.find(query, Track.class);
   }
   
   @RequestMapping(value = "/api/tracks/delete")
   public EmptyResponse deleteTrack(String id) {
      EmptyResponse r = new EmptyResponse();
      if (notAdmin()) {
         setForbidden(r);
         return r;
      }
      
      Track t = trackRepo.findById(id);
      if(t == null) {
         List<String> errs = new ArrayList();
         errs.add("No such track anymore");
         r.setErrors(errs);
         return r;
      }
      fileStorage.deleteFileById(t.getFileId());
      trackRepo.delete(id);
      
      return r;
   }
   
   @RequestMapping(value = "/api/tracks/rename")
   public EmptyResponse renameTrack(String id, String title) {
      EmptyResponse r = new EmptyResponse();
      if (notAdmin()) {
         setForbidden(r);
         return r;
      }

      Track t = trackRepo.findById(id);
      if (t == null) {
         List<String> errs = new ArrayList();
         errs.add("No such track anymore");
         r.setErrors(errs);
         return r;
      }
      t.setTitle(title);
      trackRepo.save(t);

      return r;
   }
   
   @RequestMapping(value = "/api/playlists/create")
   public PlaylistResponse createPlaylist(String title) {
      PlaylistResponse r = new PlaylistResponse();
      if (loggedInProfile == null) {
         setForbidden(r);
         return r;
      }
      if(title == null || title.length() < 3) {
         List<String> errs = new ArrayList();
         errs.add("Title is too short");
         r.setErrors(errs);
         return r;
      }
      if (title.length() > 50) {
         List<String> errs = new ArrayList();
         errs.add("Title is too long");
         r.setErrors(errs);
         return r;
      }
      Playlist pl = new Playlist();
      pl.setTitle(title);
      pl.setUserId(loggedInProfile.getId());
      pl = playlistRepo.save(pl);
      
      cachedPlaylists.put(pl.getId(), pl);
      
      return PlaylistResponse.fromPlayList(pl);
   }
   
   @RequestMapping(value = "/api/playlists")
   public List<PlaylistResponse> playlists() {
      if (loggedInProfile == null) {
         return Collections.EMPTY_LIST;
      }
      return PlaylistResponse.fromManyPlaylists(playlistRepo.findByUserId(loggedInProfile.getId()));
   }
   
   @RequestMapping(value = "/api/playlists/delete")
   public EmptyResponse deletePlaylist(String id) {
      EmptyResponse r = new EmptyResponse();
      if (loggedInProfile == null) {
         setForbidden(r);
         return r;
      }
      Playlist pl = playlistRepo.findOne(id);
      if(pl != null && (pl.getUserId().equals(loggedInProfile.getId()))) {
         playlistRepo.deleteById(id);
         cachedPlaylists.remove(pl.getId());
      }
      return r;
   }
   
   @RequestMapping(value = "/api/playlists/rename")
   public PlaylistResponse renamePlaylist(String id, String title) {
      PlaylistResponse r = new PlaylistResponse();
      if (loggedInProfile == null || id == null || id.length() == 0) {
         setForbidden(r);
         return r;
      }
      if(title == null || title.length() < 3) {
         List<String> errs = new ArrayList();
         errs.add("Title is too short");
         r.setErrors(errs);
         return r;
      }
      if(title.length() > 50) {
         List<String> errs = new ArrayList();
         errs.add("Title is too long");
         r.setErrors(errs);
         return r;
      }
      
      Playlist pl;
      if ((pl = cachedPlaylists.get(id)) == null) {
         pl = playlistRepo.findById(id);
      }
      if(pl == null || !pl.getUserId().equals(loggedInProfile.getId())) {
         setForbidden(r);
         return r;
      }
      
      pl.setTitle(title);
      pl = playlistRepo.save(pl);
      cachedPlaylists.put(pl.getId(), pl);
      
      return PlaylistResponse.fromPlayList(pl);
   }
   
   @RequestMapping("/api/playlist")
   public Playlist playlist(String id) {
      Playlist pl;
      if((pl = cachedPlaylists.get(id)) == null) {
         pl = playlistRepo.findById(id);
         if(pl != null) {
            for (Track t : pl.getTracks()) {
               t.setFileId(null);
            }
            cachedPlaylists.put(id, pl);
         }
         return pl;
      } else {
         return pl;
      }
   }
   
   @RequestMapping("/api/playlist/pushTo")
   public EmptyResponse pushTrack(String playlistId, String trackId) {
      EmptyResponse r = new EmptyResponse();
      if (loggedInProfile == null || playlistId == null) {
         setForbidden(r);
         return r;
      }
      Playlist pl;
      if((pl = cachedPlaylists.get(playlistId)) == null) {
         pl = playlistRepo.findById(playlistId);
      }
      
      if (pl == null || !pl.getUserId().equals(loggedInProfile.getId())) {
         setForbidden(r);
         return r;
      }
      Track track = trackRepo.findById(trackId);
      if(track == null) {
         List<String> errs = new ArrayList();
         errs.add("This track does not exist");
         r.setErrors(errs);
         return r;
      }
      List<Track> tracksInPlaylist = pl.getTracks();
      tracksInPlaylist.remove(track);
      tracksInPlaylist.add(track);
      playlistRepo.save(pl);
      
      return r;
   }
   
   @RequestMapping("/api/playlist/deleteFrom")
   public EmptyResponse deleteTrackFromPlaylist(String playlistId, String trackId) {
      EmptyResponse r = new EmptyResponse();
      if (loggedInProfile == null || playlistId == null) {
         setForbidden(r);
         return r;
      }
      Playlist pl;
      if ((pl = cachedPlaylists.get(playlistId)) == null) {
         pl = playlistRepo.findById(playlistId);
      }
      /*Track track = trackRepo.findById(trackId);
      if (track == null) {
         List<String> errs = new ArrayList();
         errs.add("This track does not exist");
         r.setErrors(errs);
         return r;
      }*/
      Track trackToDelete = null;
      for(Track track : pl.getTracks()) {
         if(track.getId().equals(trackId)) {
            trackToDelete = track;
         }
      }
      pl.getTracks().remove(trackToDelete);
      playlistRepo.save(pl);

      return r;
   }
   
   @RequestMapping("/api/maxTracks")
   public Integer maxTracks() {
      return maxTracks;
   }
}