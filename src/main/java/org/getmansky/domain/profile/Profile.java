/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.domain.profile;

import java.util.Objects;
import org.springframework.data.annotation.Id;

/**
 *
 * @author OlegusGetman
 */
public class Profile {

   @Id
   private String id;
   private Boolean isAdmin = false;
   private String email;
   private String encryptedPassword;

   public Boolean getIsAdmin() {
      return isAdmin;
   }

   public void setIsAdmin(Boolean isAdmin) {
      this.isAdmin = isAdmin;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getEncryptedPassword() {
      return encryptedPassword;
   }

   public void setEncryptedPassword(String encryptedPassword) {
      this.encryptedPassword = encryptedPassword;
   }

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   @Override
   public int hashCode() {
      int hash = 3;
      hash = 29 * hash + Objects.hashCode(this.id);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final Profile other = (Profile) obj;
      if (!Objects.equals(this.id, other.id)) {
         return false;
      }
      return true;
   }

   @Override
   public String toString() {
      return "Profile{" + "id=" + id + ", isAdmin=" + isAdmin + ", email=" + email + ", encryptedPassword=" + encryptedPassword + '}';
   }
}
