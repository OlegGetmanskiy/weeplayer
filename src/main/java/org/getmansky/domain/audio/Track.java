/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.domain.audio;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Objects;
import org.springframework.data.annotation.Id;

/**
 *
 * @author OlegusGetman
 */
public class Track {
   @Id
   private String id;
   @JsonIgnore
   private String fileId;
   private String title;
   @JsonIgnore
   private Float duration;

   public String getFileId() {
      return fileId;
   }

   public void setFileId(String fileId) {
      this.fileId = fileId;
   }
   
   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }
   
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public Float getDuration() {
      return duration;
   }

   public void setDuration(Float duration) {
      this.duration = duration;
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 53 * hash + Objects.hashCode(this.id);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final Track other = (Track) obj;
      if (!Objects.equals(this.id, other.id)) {
         return false;
      }
      return true;
   }
   
}
