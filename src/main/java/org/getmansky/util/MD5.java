/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OlegusGetman
 */
public class MD5 {
   public static String md5WithSalt(String str) {
      str = "all your base_are belong to_us" + str;
      try {
         byte[] bytesOfMessage = str.getBytes("UTF-8");
         MessageDigest md = MessageDigest.getInstance("MD5");
         byte[] digest = md.digest(bytesOfMessage);
         return new String(digest);
      } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
         Logger.getLogger(MD5.class.getName()).log(Level.SEVERE, null, ex);
      }
      return null;
   }
   
   public static boolean md5Equals(String str, String md5str) {
      return md5WithSalt(str).equals(md5str);
   }
}
