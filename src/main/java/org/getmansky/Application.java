/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.getmansky.domain.profile.Profile;
import org.getmansky.repo.ProfileRepository;
import org.getmansky.util.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;

@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner{
   
   @Autowired
   private ProfileRepository profileRepo;
   
   @Bean
   public EmbeddedServletContainerCustomizer containerCustomizer() {
      return new EmbeddedServletContainerCustomizer() {
         @Override
         public void customize(ConfigurableEmbeddedServletContainer container) {

            ErrorPage error401Page = new ErrorPage(HttpStatus.UNAUTHORIZED, "/index.html");
            ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/index.html");
            ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/index.html");

            container.addErrorPages(error401Page, error404Page, error500Page);
         }
      };
   }
   
   @Override
   public void run(String... args) throws Exception {
      Profile p;
      if((p = profileRepo.findByEmail("fasttty@gmail.com")) == null) {
         p = new Profile();
         p.setEmail("fasttty@gmail.com");
         p.setIsAdmin(Boolean.TRUE);
         p.setEncryptedPassword(MD5.md5WithSalt("1330"));
         profileRepo.save(p);
         Logger.getLogger(this.getClass()).log(Level.INFO, "Created admin profile: " + p);
      } else {
         Logger.getLogger(this.getClass()).log(Level.INFO, "Existing admin profile: " + p);
      }
   }
   
   public static void main(String[] args) {
      /*System.setProperty("max.tracks", "500");
      System.setProperty("server.port", "8080");
      System.setProperty("multipart.maxFileSize", "15728640");    //15 mbytes
      System.setProperty("multipart.maxRequestSize", "18728640");
      System.setProperty("spring.mvc.favicon.enabled", "false");
      System.setProperty("server.session-timeout", String.valueOf(60*60*24*7)); // 1 week*/
      SpringApplication.run(Application.class, args);
   }
}
