/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.repo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.stereotype.Service;

/**
 *
 * @author OlegusGetman
 */
@Service
public class FileStorage {
   
   private static final Logger logger = Logger.getLogger(FileStorage.class);
   
   @Autowired
   private GridFsOperations gOps;
   
   public static class File {
      public Long contentLength;
      public InputStream inputStream;
   }
   
   public static class Stats {
      private Long storageSize;

      public Long getStorageSize() {
         return storageSize;
      }

      public void setStorageSize(Long storageSize) {
         this.storageSize = storageSize;
      }
   }
   
   public String saveLocalFile(String path, String name, String mime) {
      DBObject metaData = new BasicDBObject();
      metaData.put("extra", "test");

      InputStream inputStream = null;
      try {
         inputStream = new FileInputStream(path);
         GridFSFile storedFile = gOps.store(inputStream, name, mime, metaData);
         return String.valueOf(storedFile.getId());
      } catch (FileNotFoundException e) {
         logger.log(Level.ERROR, e);
      } finally {
         if (inputStream != null) {
            try {
               inputStream.close();
            } catch (IOException e) {
               logger.log(Level.ERROR, e);
            }
         }
      }
      return null;
   }
   
   public String saveStream(InputStream is, String name, String mime) {
      DBObject metaData = new BasicDBObject();

      GridFSFile storedFile = gOps.store(is, name, mime, metaData);
      return String.valueOf(storedFile.getId());
   }
   
   public FileStorage.File findFileById(String id) {
      List<GridFSDBFile> result = gOps.find(new Query().addCriteria(Criteria.where("_id").is(id)));

      for (GridFSDBFile file : result) {
         FileStorage.File f = new FileStorage.File();
         f.contentLength = file.getLength();
         f.inputStream = file.getInputStream();
         return f;
      }
      return null;
   }
   
   public void deleteFileById(String id) {
      gOps.delete(new Query().addCriteria(Criteria.where("_id").is(id)));
   }
   
}
