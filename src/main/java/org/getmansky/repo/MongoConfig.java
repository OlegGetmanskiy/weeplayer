/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.repo;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * @author OlegusGetman
 */
@Configuration
@EnableMongoRepositories
public class MongoConfig extends AbstractMongoConfiguration {
      
   @Value("${mongo.address}")
   String mongoAddress;
   
   @Value("${mongo.user}") 
   String mongoUser;
   
   @Value("${mongo.password}") 
   String mongoPassword;
   
   @Value("${mongo.db}") 
   String mongoDbName;
   
   @Bean
   public GridFsTemplate gridFsTemplate() throws Exception {
      GridFsTemplate t = new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
      return t;
   }
   
   public @Bean
   @Override
   MongoDbFactory mongoDbFactory() throws Exception {
      UserCredentials userCredentials = new UserCredentials(mongoUser, mongoPassword);
      return new SimpleMongoDbFactory(mongo(), mongoDbName, userCredentials);
   }
   
   public @Bean
   @Override
   MongoTemplate mongoTemplate() throws Exception {
      return new MongoTemplate(mongoDbFactory());
   }

   @Override
   public Mongo mongo() throws Exception {
      return new MongoClient(mongoAddress);
   }

   @Override
   protected String getDatabaseName() {
      return mongoDbName;
   }
}
