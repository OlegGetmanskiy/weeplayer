/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.repo;

import org.getmansky.domain.audio.Track;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author OlegusGetman
 */
public interface TrackRepository extends MongoRepository<Track, String> {
   @Override
   public Track save(Track t);
   public Track findById(String id);
   public Track findByTitle(String title);
   public Long removeById(String id);
}
