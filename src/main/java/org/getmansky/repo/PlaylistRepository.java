/*
 * Copyright (C) 2015 Oleg Getmansky aka OlegusGetman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.getmansky.repo;

import java.util.List;
import org.getmansky.domain.playlist.Playlist;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author OlegusGetman
 */
public interface PlaylistRepository extends MongoRepository<Playlist, String> {
   public Playlist findById(String id);
   public List<Playlist> findByUserId(String userId);
   @Override
   public Playlist save(Playlist playlist);
   public Long deleteById(String id);
}
